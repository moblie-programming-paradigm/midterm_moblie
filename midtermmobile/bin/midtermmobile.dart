import 'dart:io';
import 'dart:math';

void main() {
  String? input = stdin.readLineSync();
  String string = input!;
  var list = string.split(' ');
  List<String> postfix = toPostfix(list);
  evaluatePostFix(postfix);
}

void evaluatePostFix(List<String> postfix) {
  List<double> values = [];
  for (var token in postfix) {
    if (isNumeric(token)) {
      values.add(double.parse(token));
    } else {
      double right = values.removeLast();
      double left = values.removeLast();
      switch (token) {
        case "+":
          values.add(left + right);
          break;
        case "-":
          values.add(left - right);
          break;
        case "*":
          values.add(left * right);
          break;
        case "/":
          values.add(left / right);
          break;
        default:
      }
    }
  }
  print(values);
}

List<String> toPostfix(List<String> list) {
  List<String> postfix = [];
  List<String> operators = [];
  for (var token in list) {
    if (isNumeric(token)) {
      postfix.add(token);
    }

    if (isOperator(token)) {
      while (!operators.isEmpty &&
          operators.last != "(" &&
          precedence(token) < precedence(operators.last)) {
        postfix.add(operators.removeLast());
      }
      operators.add(token);
    }
    if (token == "(") {
      operators.add(token);
    }
    if (token == ")") {
      while (operators.last != "(") {
        postfix.add(operators.removeLast());
      }
      operators.removeLast();
    }
  }
  while (operators.isNotEmpty) {
    postfix.add(operators.removeLast());
  }
  return postfix;
}

precedence(String token) {
  switch (token) {
    case "+":
    case "-":
      return 1;
    case "*":
    case "/":
      return 2;
    case "(":
      return 3;
  }
  return -1;
}

bool isNumeric(String str) {
  try {
    var value = double.parse(str);
  } on FormatException {
    return false;
  } finally {
    return true;
  }
}

bool isOperator(String token) {
  if (token == "+" ||
      token == "-" ||
      token == "*" ||
      token == "/" ||
      token == "^") {
    return true;
  }
  return false;
}
